<?php

use FormBuilder\Factories\FieldsFactory;
use FormBuilder\Factories\IdFactory;
use FormBuilder\Factories\KeyFactory;
use FormBuilder\Factories\NameFactory;
use FormBuilder\Factories\OptionsFactory;
use FormBuilder\Factories\PropertyFactory;
use FormBuilder\Factories\RulesFactory;

return [
    /**
     * A map of the available field types
     */
    'field_types' => [
        'text' => TextType::class,
    ],

    /**
     * A map of the available form types
     */
    'field_types' => [
        'default' => TextType::class,
    ],

    /**
     * The property factory
     */
    'property_factory' => PropertyFactory::class,

    /**
     * The field entity class
     */
    'field_entity' => Field::class,

    /**
     * The form entity class
     */
    'form_entity' => Form::class,
];
