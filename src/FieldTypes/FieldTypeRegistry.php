<?php

namespace FormBuilder\FieldType;

use FormBuilder\Common\ImmutableRegistry;
use FormBuilder\FieldTypes\FieldType;

class FieldTypeRegistry extends ImmutableRegistry
{
  /**
   * (typed)
   * @return FieldType
   */
  public function get(string $key)
  {
    return parent::get($key);
  }

  protected function register(string $key, $item)
  {
    $this->items[$key] = new $item($key);
  }
}
