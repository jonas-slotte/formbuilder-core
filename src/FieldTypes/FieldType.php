<?php

namespace FormBuilder\FieldTypes;

use FormBuilder\Properties\Type;

/**
 * A field's "engine" core
 */
abstract class FieldType
{
    protected $type;

    public function __construct(Type $type)
    {
        $this->type = $type;
    }

    /**
     * Get an inverse property map:
     * string key is mapped to a set of property validation rules.
     *
     * @return array
     */
    public function getPropertyRules()
    {
        return [];
    }

    public function type()
    {
        return $this->type;
    }

    public function get()
    {
        return $this->type()->get();
    }

    public function defaultRules()
    {
        return [];
    }

    public function __toString()
    {
        return (string) $this->get();
    }
}
