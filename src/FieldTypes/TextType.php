<?php

namespace FormBuilder\FieldTypes;


class TextType extends FieldType
{
  public function defaultRules()
  {
    return [
      'required',
      'string',
      'max:255'
    ];
  }
}
