<?php

namespace FormBuilder\FieldTypes;



class TextAreaType extends FieldType
{
  public function defaultRules()
  {
    return [
      'required',
      'string',
      'max:1000'
    ];
  }
}
