<?php

namespace FormBuilder\FieldTypes;



class EmailType extends FieldType
{
  public function defaultRules()
  {
    return [
      'required',
      'string',
      'email',
      'max:255'
    ];
  }
}
