<?php

namespace App\Providers;

use FormBuilder\Factories\PropertyFactory;
use FormBuilder\Factories\PropertyFactoryRegistry;
use FormBuilder\FieldType\FieldTypeRegistry;
use FormBuilder\FormTypes\FormTypeRegistry;
use FormBuilder\Properties\PropertyRegistry;
use FormBuilder\ServiceLocator;
use Illuminate\Support\ServiceProvider;

class FormbuilderServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(FormTypeRegistry::class, function ($app) {
            return new FormTypeRegistry(config('formbuilder.form_types'));
        });
        $this->app->singleton(FieldTypeRegistry::class, function ($app) {
            return new FieldTypeRegistry(config('formbuilder.field_types'));
        });
        // $this->app->singleton(PropertyFactoryRegistry::class, function ($app) {
        //     return new PropertyFactoryRegistry(config('formbuilder.properties'));
        // });
        $this->app->singleton(PropertyFactory::class, function ($app) {
            return new PropertyFactory(config('formbuilder.property_factory'));
        });
        $this->app->singleton(ServiceLocator::class, function ($app) {
            return new ServiceLocator();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
