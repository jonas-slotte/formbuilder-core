<?php

namespace FormBuilder\Factories;

use App\Providers\ArrayFactory;
use FormBuilder\Properties\Fields;
use Webmozart\Assert\Assert;

class PropertyFactory
{
    /**
     * @var ArrayFactory
     */
    protected $arrayFactory;

    public function __construct(ArrayFactory $factory){
        $this->arrayFactory = $factory;
    }

    public function parse(string $key, $input)
    {
        $methodName = "parse".ucfirst($key);
        if(!method_exists($this,$methodName)){
            throw new FactoryException("The 'key' must be handled by '$methodName', which does not exist.");
        }
        return $this->$methodName($input);
    }

    public function parseField($input)
    {
        Assert::isNonEmptyList($input);
        return new Fields($input);
    }

    public function __call($name, $arguments)
    {
        if()
    }
}
