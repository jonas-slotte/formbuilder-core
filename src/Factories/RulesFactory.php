<?php

namespace FormBuilder\Factories;

use Webmozart\Assert\Assert;

class RulesFactory extends PropertyFactory
{
    public function parse($input)
    {
        Assert::isNonEmptyList($input);
        return $input;
    }
}
