<?php

namespace FormBuilder\Factories;

use FormBuilder\Common\ImmutableRegistry;

class PropertyFactoryRegistry extends ImmutableRegistry
{
  /**
   * (typed)
   * @return PropertyFactory
   */
  public function get(string $key)
  {
    return parent::get($key);
  }

  protected function register(string $key, $item)
  {
    /**
     * This uses container for registration
     */
    $this->items[$key] = app()->make($item);
  }
}
