<?php

namespace FormBuilder\Factories;

use FormBuilder\Properties\Type;
use Webmozart\Assert\Assert;

class TypeFactory extends PropertyFactory
{
    public function parse($input)
    {
        Assert::stringNotEmpty($input);
        return new Type($input);
    }
}
