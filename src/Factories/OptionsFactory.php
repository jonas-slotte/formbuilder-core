<?php

namespace FormBuilder\Factories;

use FormBuilder\Properties\Options;
use Webmozart\Assert\Assert;

class OptionsFactory extends PropertyFactory
{
    public function parse($input)
    {
        Assert::isNonEmptyList($input);
        return new Options($input);
    }
}
