<?php

namespace FormBuilder\Factories;

use FormBuilder\Properties\Id;
use Webmozart\Assert\Assert;

class IdFactory extends PropertyFactory
{
    public function parse($input)
    {
        Assert::stringNotEmpty($input);
        return new Id($input);
    }
}
