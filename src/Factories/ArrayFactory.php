<?php

namespace App\Providers;

use FormBuilder\Core\Field;
use FormBuilder\Core\Form;
use FormBuilder\Entities\FieldCollection;
use FormBuilder\Factories\FactoryException;
use FormBuilder\Properties\PropertyAssertions;
use FormBuilder\Properties\PropertyCollection;
use FormBuilder\ServiceLocator;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;

class ArrayFactory
{
    /**
     * @var ServiceLocator
     */
    protected $services;

    public function __construct(ServiceLocator $serviceLocator)
    {
        $this->services = $serviceLocator;
    }

    /**
     * @param array $data
     * @return Form
     */
    public function form(array $data)
    {
        if (sizeof($data) == 0) {
            throw new FactoryException("The forms must contain at least one field");
        }
        $fields = new FieldCollection();
        foreach ($data as $field) {
            $fields->push($this->field($field));
        }
        return new Form($fields);
    }

    /**
     * @param array $data
     * @return Field
     */
    public function field(array $data)
    {
        $type = $data["type"] ?? null;
        if ($type == null) {
            throw new FactoryException("The field must contain a 'type' key");
        }
        $data = collect($data)->except(['type']); //Except the type, since it is absolutely required

        $fieldType = $this->services->fieldTypeRegistry()->get($type);

        Validator::make($data, $fieldType->getPropertyRules())->validate();

        $properties = new PropertyCollection();
        $propertyFactory = $this->services->propertyFactory();
        foreach ($data as $key => $payload) {
            try {
                $properties->push($propertyFactory->parse($key, $payload));
            } catch (InvalidArgumentException $i) {
                throw new FactoryException("Property '$property': " . $i->getMessage(), 0, $i);
            }
        }
        return new Field($fieldType, $properties);
    }
}
