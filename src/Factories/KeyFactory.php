<?php

namespace FormBuilder\Factories;

use FormBuilder\Properties\Key;
use Webmozart\Assert\Assert;

class KeyFactory extends PropertyFactory
{
    public function parse($input)
    {
        Assert::stringNotEmpty($input);
        return new Key($input);
    }
}
