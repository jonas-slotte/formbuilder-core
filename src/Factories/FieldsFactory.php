<?php

namespace FormBuilder\Factories;

use FormBuilder\Properties\Fields;
use Webmozart\Assert\Assert;

class FieldsFactory extends PropertyFactory
{
    public function parse($input)
    {
        Assert::isNonEmptyList($input);
        return new Fields($input);
    }
}
