<?php

namespace FormBuilder\Factories;

use FormBuilder\Properties\Name;
use Webmozart\Assert\Assert;

class TranslatedNameFactory extends PropertyFactory
{
    public function parse($input)
    {
        Assert::stringNotEmpty($input);
        return new Name(trans($input));
    }
}
