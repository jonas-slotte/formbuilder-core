<?php

namespace FormBuilder\Factories;

use FormBuilder\Properties\Name;
use Webmozart\Assert\Assert;

class NameFactory extends PropertyFactory
{
    public function parse($input)
    {
        Assert::stringNotEmpty($input);
        return new Name($input);
    }
}
