<?php

namespace FormBuilder;

use FormBuilder\Factories\PropertyFactory;
use FormBuilder\FieldType\FieldTypeRegistry;
use FormBuilder\FormTypes\FormTypeRegistry;
use FormBuilder\Factories\PropertyFactoryRegistry;
use FormBuilder\Properties\PropertyRegistry;

/**
 * Configuration Service locator.
 * Reduces parameter passing and aids testing.
 */
class ServiceLocator
{
    /**
     * @return FieldTypeRegistry
     */
    public function fieldTypeRegistry()
    {
        return app()->make(FieldTypeRegistry::class);
    }

    /**
     * @return FormTypeRegistry
     */
    public function formTypeRegistry()
    {
        return app()->make(FormTypeRegistry::class);
    }

    /**
     * @return PropertyRegistry
     */
    public function propertyRegistry()
    {
        return app()->make(PropertyRegistry::class);
    }

    /**
     * @return PropertyFactory
     */
    public function propertyFactory()
    {
        return app()->make(PropertyFactory::class);
    }

    /**
     * @return PropertyFactoryRegistry
     */
    public function propertyFactoryRegistry()
    {
        return app()->make(PropertyFactoryRegistry::class);
    }
    /**
     * @param string $fieldType
     * @return FieldType
     */
    public function fieldType(string $fieldType)
    {
        return $this->fieldTypeRegistry()->get($fieldType);
    }

    /**
     * @param string $formType
     * @return FormType
     */
    public function formType(string $formType)
    {
        return $this->formTypeRegistry()->get($formType);
    }
}
