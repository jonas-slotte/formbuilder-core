<?php

use Ramsey\Uuid\Uuid;

if (!function_exists('uuidString')) {

    /**
     * @return string
     */
    function uuidString()
    {
        return Uuid::uuid4()->toString();
    }
}
