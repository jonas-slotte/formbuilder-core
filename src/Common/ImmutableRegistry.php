<?php

namespace FormBuilder\Common;

abstract class ImmutableRegistry
{
  /**
   * @var array
   */
  protected $items = [];

  /**
   * Initialize with items.
   *
   * @param array $map
   */
  public function __construct(array $map = [])
  {
    foreach ($map as $key => $value) {
      $this->register($key, $value);
    }
  }

  protected abstract function register(string $key, $item);


  /**
   * @param string $key
   */
  public function get(string $key)
  {
    $item = $this->items[$key] ?? null;
    if ($item === null) {
      $class = get_class($this);
      throw new \Exception("$class does not contain '$key'");
    }
    return $this->items[$key];
  }

  /**
   * @return array
   */
  public function items()
  {
    return $this->items;
  }
}
