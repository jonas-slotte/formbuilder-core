<?php

namespace FormBuilder\Core;

use FormBuilder\FieldTypes\FieldType;
use FormBuilder\Properties\PropertyCollection;

class Field
{
    /**
     * @var PropertyCollection
     */
    protected $properties;

    /**
     * @var FieldType
     */
    protected $fieldType;

    public function __construct(FieldType $fieldType, PropertyCollection $properties)
    {
        $this->fieldType = $fieldType;
        $this->properties = $properties;
    }

    public function fieldType()
    {
        return $this->fieldType;
    }

    public function properties()
    {
        return $this->properties;
    }

    public function is(Field $other)
    {
        return $this->properties()->id()->is($other->properties()->id());
    }
}
