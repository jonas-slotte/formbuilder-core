<?php

namespace FormBuilder\Core;

use FormBuilder\Entities\FieldCollection;

class Form
{
    /**
     * @var FieldCollection
     */
    protected $fields;

    public function __construct(FieldCollection $fields)
    {
        $this->fields = $fields;
    }

    public function fields()
    {
        return $this->fields;
    }
}
