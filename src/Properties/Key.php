<?php

namespace FormBuilder\Properties;

class Key extends Property
{
    public function __construct(string $input)
    {
        $this->input = $input;
    }
}
