<?php

namespace FormBuilder\Properties;

class Type extends Property
{
    public function __construct(string $input)
    {
        $this->input = $input;
    }

    public function get()
    {
        return $this->input;
    }
}
