<?php

namespace FormBuilder\Properties;

class Id extends Property
{
    public function __construct(string $input)
    {
        $this->input = $input;
    }
}
