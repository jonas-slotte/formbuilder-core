<?php

namespace FormBuilder\Properties;

class Options extends Property
{
    public function __construct(array $input)
    {
        $this->input = $input;
    }
}
