<?php

namespace FormBuilder\Properties;

use FormBuilder\Common\ImmutableRegistry;

class PropertyRegistry extends ImmutableRegistry
{
  /**
   * (typed)
   * @return Property
   */
  public function get(string $key)
  {
    return parent::get($key);
  }

  protected function register(string $key, $item)
  {
    $this->items[$key] = $item;
  }
}
