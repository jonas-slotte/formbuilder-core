<?php

namespace FormBuilder\Properties;

use Webmozart\Assert\Assert;

class Name extends Property
{
    public function __construct(string $input)
    {
        $this->input = $input;
    }
}
