<?php

namespace FormBuilder\Properties;

use Illuminate\Support\Collection;

class PropertyCollection extends Collection
{
    /**
     * Get a property if it exists, otherwise throw exception
     *
     * @param string $key
     * @return mixed
     */
    public function getOrThrow(string $key)
    {
        if ($this->get($key) === null) {
            throw new PropertyNotFoundException("The property '$key' was not found. Are you sure the field should contain it?");
        }
        return $this->get($key);
    }

    public function id()
    {
        return $this->getOrThrow('id');
    }

    public function name()
    {
        return $this->getOrThrow('name');
    }

    public function key()
    {
        return $this->getOrThrow('key');
    }

    public function version()
    {
        return $this->getOrThrow('version');
    }

    public function type()
    {
        return $this->getOrThrow('type');
    }

    public function options()
    {
        return $this->getOrThrow('options');
    }
}
