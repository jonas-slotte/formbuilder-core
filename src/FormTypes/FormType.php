<?php

namespace FormBuilder\FormTypes;

class FormType
{
    protected $type;

    public function __construct($input)
    {
        $this->type = $input;
    }

    public function get()
    {
        return $this->type;
    }

    public function __toString()
    {
        return (string) $this->get();
    }
}
