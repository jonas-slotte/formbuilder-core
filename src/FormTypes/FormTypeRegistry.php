<?php

namespace FormBuilder\FormTypes;

use FormBuilder\Common\ImmutableRegistry;

class FormTypeRegistry extends ImmutableRegistry
{
  /**
   * (typed)
   * @return FormType
   */
  public function get(string $key)
  {
    return parent::get($key);
  }

  protected function register(string $key, $item)
  {
    $this->items[$key] = new $item($key);
  }
}
